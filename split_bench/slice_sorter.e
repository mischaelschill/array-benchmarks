note
	description: "The {ARRAY_SORTER} sorts arrays, either sequential or concurrently. It currently uses a quicksort algorithm."
	author: "Mischael Schill"
	date: "$Date$"
	revision: "$Revision$"

class
	SLICE_SORTER[G -> COMPARABLE]

create
	make, make_internal

feature {NONE}

	make (a_data: SLICE[G]; a_workers: INTEGER)
		-- Sorts `a_data' using `a_workers' workers
		do
			workers := a_workers
			data := a_data
		ensure
			workers = a_workers
			data = a_data
		end

	make_internal (a_workers: INTEGER)
		do
			workers := a_workers
		end

feature -- Access

	workers: INTEGER

	data: SLICE[G]

feature
	sort
		local
			middle, i, j: INTEGER
			pivot, tmp: G
			sorter: SLICE_SORTER[G]
			separate_sorter1, separate_sorter2: separate SLICE_SORTER[G]
		do
			if data.count > 1 then
				-- Partition
				middle := data.count // 2 + data.lower
				pivot := data[middle]
				data[middle] := data[data.upper]
				data[data.upper] := pivot
				from
					i := data.lower
					j := data.lower
				until
					i = data.upper
				loop
					if data[i] < pivot then
						tmp := data[i]
						data[i] := data[j]
						data[j] := tmp
						j := j + 1
					end
					i := i + 1
				end

				data[data.upper] := data[j]
				data[j] := pivot

				if workers > 1 then
					create separate_sorter1.make_internal (workers // 2)
					create separate_sorter2.make_internal (workers // 2)
					separate_sort(separate_sorter1, separate_sorter2, j)
				else
					sequential_sort (data.lower, j - 1)
					sequential_sort (j + 1, data.upper)
				end
			end
--			print ("Sorted " + data.lower.out + " to " + data.upper.out + "%N")
		end


feature {NONE} -- Implementation

	separate_sort (separate_sorter1, separate_sorter2: separate SLICE_SORTER[G]; j: INTEGER)
		local
			l, r: INTEGER
		do
			l := j - data.lower
			r := data.upper - j
			separate_sorter1.slice_top (data, l)
			separate_sorter2.slice_bottom (data, r)
			separate_sorter1.sort
			separate_sorter2.sort
			create data.merge (data, separate_sorter1.data)
			create data.merge (data, separate_sorter2.data)
		end

	separate_data (separate_sorter: separate SLICE_SORTER[G]): separate SLICE[G]
		do
			Result := separate_sorter.data
		end

	sequential_sort (a_lower, a_upper: INTEGER)
		-- For efficient sequential sorting (no need for additional objects)
		local
			count, middle, i, j: INTEGER
			pivot, tmp: G
		do
			count := a_upper - a_lower + 1
			if count > 1 then
				-- Partition
				middle := count // 2 + a_lower
				pivot := data[middle]
				data[middle] := data[a_upper]
				data[a_upper] := pivot
				from
					i := a_lower
					j := a_lower
				until
					i = a_upper
				loop
					if data[i] < pivot then
						tmp := data[i]
						data[i] := data[j]
						data[j] := tmp
						j := j + 1
					end
					i := i + 1
				end

				data[a_upper] := data[j]
				data[j] := pivot

				sequential_sort (a_lower, j - 1)
				sequential_sort (j + 1, a_upper)
			end
		end

feature {SLICE_SORTER}

	slice_top (a_data: separate SLICE[G]; n: INTEGER)
		-- Sorts `a_data' using `a_workers' workers
		do
			create data.slice_head (n, a_data)
		end

	slice_bottom (a_data: separate SLICE[G]; n: INTEGER)
		-- Sorts `a_data' using `a_workers' workers
		do
			create data.slice_tail (n, a_data)
		end

end
