note
	description : "Benchmark for array splitting"
	date        : "$Date$"
	revision    : "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization	
	make
			-- Run application.
		local
			data: SLICE[INTEGER]
			matrix1, matrix2: MATRIX[REAL_64]
			start, finish: TIME
			duration: TIME_DURATION
			i, j: INTEGER
			a, b: INTEGER
			rand: RANDOM
		do
			if argument_count > 0 then
				Num_workers := argument (1).to_integer
			else
				Num_workers := 8
			end

			-- Randomize data
			create data.make_filled (0, 1, Data_size)
			from
				i := 1
				create rand.set_seed (42)
				rand.start
			until
				i > Data_size
			loop
				data[i] := rand.item
				rand.forth
				i := i + 1
			end

			-- Sort data
			create start.make_now
			sort(data)
			create finish.make_now
			duration := finish.duration - start.duration
			io.put_double(duration.fine_second + 60 * duration.minute + 60 * 60 * duration.hour)
			io.put_character (' ')


			create matrix1.make_filled (0, Matrix_rows, Matrix_columns)
			create matrix2.make_filled (0, Matrix_columns, Matrix_rows)
			-- Randomize data
			from
				i := 1
			until
				i > Matrix_rows
			loop
				from
					j := 1
				until
					j > Matrix_columns
				loop
					a := rand.item
					rand.forth
					b := rand.item
					rand.forth
					if b = 0 then
						b := 1
					end
					matrix1.put (a / b, i, j)
					a := rand.item
					rand.forth
					b := rand.item
					rand.forth
					if b = 0 then
						b := 1
					end
					matrix2.put (a / b, j, i)
					j := j + 1
				end
				i := i + 1
			end

			create start.make_now
			matrix1.set_workers (Num_workers)
			matrix2.set_workers (Num_workers)
			matrix1 := matrix1.multiply (matrix2)
			create finish.make_now
			duration := finish.duration - start.duration
			io.put_double(duration.fine_second + 60 * duration.minute + 60 * 60 * duration.hour)
			io.put_character (' ')
			io.put_new_line
		end

	sort (data: SLICE[INTEGER])
		local
			sorter: SLICE_SORTER[INTEGER]
		do
			create sorter.make (data, Num_workers)
			sorter.sort
			if sorter.data.is_empty then
				print ("WTF%N")
			end
		end

feature -- Constants

	Data_size: INTEGER = 100000000
		-- Size of the input data

	Matrix_rows: INTEGER = 2000
	Matrix_columns: INTEGER = 800

	Num_workers: INTEGER
		-- Number of workers

end
