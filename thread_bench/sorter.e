note
	description: "Summary description for {SORTER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	SORTER [G -> COMPARABLE]

inherit
	THREAD
		rename make as make_thread end

create
	make

feature
	make (a_data: MY_ARRAY[G]; a_lower, a_upper, a_workers: INTEGER)
		do
			make_thread
			workers := a_workers
			data := a_data
			lower := a_lower
			upper := a_upper
		end

	data: MY_ARRAY[G]

	lower, upper: INTEGER

	workers :INTEGER

	execute
		do
			sort (lower, upper, workers)
		end

	sort (a_lower, a_upper, a_workers: INTEGER)
		local
			count, middle, i, j: INTEGER
			pivot, tmp: G
			sorter1, sorter2: SORTER[G]
		do
			count := a_upper - a_lower + 1
			if count > 1 then
				-- Partition
				middle := count // 2 + a_lower
				pivot := data[middle]
				data[middle] := data[a_upper]
				data[a_upper] := pivot
				from
					i := a_lower
					j := a_lower
				until
					i = a_upper
				loop
					if data[i] < pivot then
						tmp := data[i]
						data[i] := data[j]
						data[j] := tmp
						j := j + 1
					end
					i := i + 1
				end

				data[a_upper] := data[j]
				data[j] := pivot

				if a_workers > 1 then
					create sorter1.make (data, a_lower, j - 1, a_workers // 2)
					sorter1.launch
					create sorter2.make (data, j + 1, a_upper, a_workers // 2)
					sorter2.launch
					sorter1.join
					sorter2.join
				else
					sequential_sort (a_lower, j - 1)
					sequential_sort (j + 1, a_upper)
				end
			end
		end

	sequential_sort (a_lower, a_upper: INTEGER)
		-- For efficient sequential sorting (no need for additional objects)
		local
			count, middle, i, j: INTEGER
			pivot, tmp: G
		do
			count := a_upper - a_lower + 1
			if count > 1 then
				-- Partition
				middle := count // 2 + a_lower
				pivot := data[middle]
				data[middle] := data[a_upper]
				data[a_upper] := pivot
				from
					i := a_lower
					j := a_lower
				until
					i = a_upper
				loop
					if data[i] < pivot then
						tmp := data[i]
						data[i] := data[j]
						data[j] := tmp
						j := j + 1
					end
					i := i + 1
				end

				data[a_upper] := data[j]
				data[j] := pivot

				sequential_sort (a_lower, j - 1)
				sequential_sort (j + 1, a_upper)
			end
		end
end
