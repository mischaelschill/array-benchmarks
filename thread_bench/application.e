note
	description : "EiffelThreads benchmark"
	date        : "$Date$"
	revision    : "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			data: MY_ARRAY[INTEGER]
			start, finish: TIME
			duration: TIME_DURATION
			i, j, a, b: INTEGER
			sorter: SORTER[INTEGER]
			random_sequence: RANDOM
			max: INTEGER
			matrix1, matrix2, matrix3: MY_ARRAY2[REAL_64]
			mult: MATRIX_MULTIPLIER[REAL_64]
			mults: ARRAYED_LIST[MATRIX_MULTIPLIER[REAL_64]]
			rand: RANDOM
		do
			if argument_count > 0 then
				Num_workers := argument (1).to_integer
			else
				Num_workers := 8
			end

			create data.make (1, Data_size)

			from
				i := 1
				create rand.set_seed (42)
				rand.start
			until
				i > Data_size
			loop
				data[i] := rand.item
				rand.forth
				i := i + 1
			end

			create start.make_now
				create sorter.make (data, data.lower, data.upper, Num_workers)
				sorter.execute
			create finish.make_now
			duration := finish.duration - start.duration
			io.put_double(duration.fine_second + 60 * duration.minute + 60 * 60 * duration.hour)
			io.put_character (' ')


			create matrix1.make_filled (0, Matrix_rows, Matrix_columns)
			create matrix2.make_filled (0, Matrix_columns, Matrix_rows)
			-- Randomize data
			from
				i := 1
			until
				i > Matrix_rows
			loop
				from
					j := 1
				until
					j > Matrix_columns
				loop
					a := rand.item
					rand.forth
					b := rand.item
					rand.forth
					if b = 0 then
						b := 1
					end
					matrix1.put (a / b, i, j)
					a := rand.item
					rand.forth
					b := rand.item
					rand.forth
					if b = 0 then
						b := 1
					end
					matrix2.put (a / b, j, i)
					j := j + 1
				end
				i := i + 1
			end


			create start.make_now
				create mults.make (Num_workers)
				create matrix3.make(Matrix_rows, Matrix_rows)
				across
					1 |..| Num_workers as iter
				loop
					create mult.set_data (matrix1, matrix2, matrix3, (iter.item - 1)*(Matrix_rows / Num_workers).ceiling + 1,
						((iter.item)*(Matrix_rows / Num_workers).ceiling).min(Matrix_rows), Matrix_rows, Matrix_columns)
					mult.launch
					mults.extend (mult)
				end

				across
					mults as iter
				loop
					iter.item.join
				end
			create finish.make_now
			duration := finish.duration - start.duration
			io.put_double(duration.fine_second + 60 * duration.minute + 60 * 60 * duration.hour)
			io.put_character (' ')
			io.put_new_line
	end

	Num_workers: INTEGER
		-- Number of workers

	Data_size: INTEGER = 100

	Matrix_rows: INTEGER = 2000
	Matrix_columns: INTEGER = 800

end
