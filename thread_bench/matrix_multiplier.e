note
	description: "Summary description for {MATRIX_MULTIPLIER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	MATRIX_MULTIPLIER[G -> NUMERIC]

inherit
	THREAD

create
	set_data

feature
	lower, upper, columns, inner: INTEGER
	left, right, output: MY_ARRAY2[G]

	set_data (a_left, a_right, a_output: MY_ARRAY2[G]; a_lower, a_upper, a_columns, a_inner: INTEGER)
		do
			make
			left := a_left
			right := a_right
			output := a_output
			lower := a_lower
			upper := a_upper
			columns := a_columns
			inner := a_inner
		end

	execute
		local
			k, i, j, m, n, o: INTEGER
		do
			from
				i := lower
				m := upper
			until
				i > m
			loop
				from
					j := 1
					n := columns
				until
					j > n
				loop
					from
						k := 1
						o := inner
					until
						k > o
					loop
						output[i, j] := output[i,j] + left[i, k] * right[k, j]
						k := k + 1
					end
					j := j + 1
				end
				i := i + 1
			end
		end

end
